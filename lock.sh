#!/bin/bash
# bate um print da tela
scrot /tmp/screen.png
#converte o print em escala
convert /tmp/screen.png -scale 10% -scale 1000% /tmp/screen.png
# embaralha a imagem
convert /tmp/screen.png ~/.config/i3/lock.png -gravity center -composite -matte /tmp/lock.png
# insere a imagem de chave no centro da imagem embaralhada
[[ -f $1 ]] && convert /tmp/lock.png $1 -gravity center -composite -matte /tmp/lock.png
# desliga o spotify
dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Stop
# bloqueia a tela
i3lock -u -i /tmp/lock.png
# apaga as imagens criadas
rm /tmp/screen.png
rm /tmp/lock.png
